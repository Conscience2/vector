#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utest.h"
#include "../src/vector.h"

char msg[100];
char cletters[100];
Vector v;

static char * test_VectorNew()
{
	int size = sizeof(char);
	int initialAllocation = 10;

	VectorNew(&v, size, initialAllocation); //function testing
	sprintf(msg, "elemSize must be %d\n", size);
	assert_test(msg, v.elemSize == size);

	sprintf(msg, "initialAllocation must be %d\n", initialAllocation);
	assert_test(msg, v.initialAllocation == initialAllocation);

	sprintf(msg, "nElems  must be %d\n", 0);
	assert_test(msg, v.nelem == 0);

	sprintf(msg, "elem must have enough space for %d\n", initialAllocation*size);
	assert_test(msg, malloc_usable_size(v.elems)); //assert_test está en utest.h

	return 0;
}

static char * test_VectorAppend(){
	char c;
	int i = 0;
	for (c='a'; c<='z'; c++, i++)
	{
		VectorAppend(&v, &c);
		cletters[i] = c;
	}
	cletters[i] = '\0';

	sprintf(msg, "Vector must content %s \n", cletters);
	assert_test(msg, strncmp(v.elems, cletters, i-1) ==0);
	return 0;
}

static char * test_VectorLength(){
	sprintf(msg, "Vector length must be %d \n", v.nelem);
	assert_test(msg, VectorLength(&v) == v.nelem);
	return 0;
}

static char * test_VectorReplace(){
	char c = 'm';
	int position = 1;
	cletters[position] = 'm';
	VectorReplace(&v, &c, position);

	sprintf(msg, "position is not valid\n");
	assert_test(msg, position >= 0 && position < v.nelem);

	sprintf(msg, "Vector must contain %c in position %d \n", c, position);
	assert_test(msg, *((char *) v.elems + position * v.elemSize) == cletters[position]);
	return 0;

}

static char * test_VectorInsert(){
	int position = 0;
	char c = 'z';

	VectorInsert(&v, &c, position);
	sprintf(msg, "Vector must content %c in position %d \n", c, position);
	assert_test(msg, *((char *) v.elems + position * v.elemSize) == c);

	position = 1;
	c = 'a';

	sprintf(msg, "Vector must content %c in position %d \n", c, position);
	assert_test(msg, *((char *) v.elems + position * v.elemSize) == c);

	sprintf(msg, "position is not valid\n");
	assert_test(msg, position >= 0 && position < v.nelem);

	return 0;
}

static char * test_VectorNth(){
	char n = 'a';
	int position = 2;

	VectorNth(&v, &n, position);

	sprintf(msg, "Elemaddr must contain %c\n", *((char *) v.elems + position * v.elemSize));
	assert_test(msg, *((char *) v.elems + position * v.elemSize) == n);


	sprintf(msg, "Position must be greater than -1\n");
	assert_test(msg, position >=0);

	sprintf(msg, "Position must be less than %d\n", v.nelem-1);
	assert_test(msg, position < v.nelem-1);

	return 0;
}

void acumulative(void *elem, void *total){
	int *a, *b;
	a = (int *)elem;
	b = (int *)total;
	*b += *a;
}

void printInt(void *elem, void*aux){
	printf("%d ", *((int*)elem));
}

static char * test_VectorMap(){
	Vector numbers;
	int sizeElem = sizeof(int);
	int iniSize = 10;
	int result = 0;
	int total = 0;
	int i;

	VectorNew(&numbers, sizeElem, iniSize);

	for(i = 0; i < 20; i++){
		VectorAppend(&numbers, &i);
		result += i;
	}

	VectorMap(&numbers,acumulative, (void *)&total);

	printf("[");
	VectorMap(&numbers,printInt, NULL);
	printf("]\n");

	sprintf(msg, "Total must be %d\n", result);
 	assert_test(msg, total == result);

	return 0;
}

void printString(void *elem, void *aux){
	char *s = *(char **)elem;
	printf(" %s ", s);
}

int cmpStrings(const void *elem1, const void *elem2){
	return strcmp(*(char **)elem1, *(char **)elem2);
}

static char * test_VectorSort(){
	Vector names;
	int sizeElem = sizeof(char *);
	int iniSize = 10;
	char *dir;

	VectorNew(&names, sizeElem, iniSize);

	dir = strdup("Miguel");
	VectorAppend(&names, &dir);
	dir = strdup("Angelica");
	VectorAppend(&names, &dir);
	dir = strdup("Juan");
	VectorAppend(&names, &dir);
	printf("[");
	VectorMap(&names,printString, NULL);
	printf("]\n");

	VectorSort(&names, cmpStrings);
	printf("[");
	VectorMap(&names,printString, NULL);
	printf("]\n");

	assert_test("Failed", 0 == 0);

	return 0;
}

int main(){
	run_test("Vector new test: ", test_VectorNew);
	run_test("testing VectorAppend", test_VectorAppend);
	run_test("testing VectorLength", test_VectorLength);
	run_test("testing VectorReplace", test_VectorReplace);
	run_test("testing VectorInsert", test_VectorInsert);
	run_test("testing VectorNth", test_VectorNth);
	run_test("testing VectorMap", test_VectorMap);
	run_test("testing VectorSort", test_VectorSort);

	return 0;
}
