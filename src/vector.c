#include <stdlib.h>
#include <string.h>
#include "vector.h"

void VectorNew(Vector *v, int elemSize, int initialAllocation){
	v->nelem = 0;
	v->elems = malloc(elemSize*initialAllocation);
	v->initialAllocation = initialAllocation;
	v->elemSize = elemSize;

	return;
}

void VectorAppend(Vector *v, const void *elemAddr){
	if(v->nelem	== v->initialAllocation){
		v->initialAllocation *= 2;
		v->elems = realloc(v->elems, v->initialAllocation * v->elemSize);
	}
	memcpy((char *) v->elems + v->nelem * v->elemSize, elemAddr, v->elemSize);
	v->nelem++;
	return;
}

int VectorLength(const Vector *v){
	return v->nelem;
}

void VectorReplace(Vector *v, const void *elemAddr, int position){
	memcpy((char *) v->elems + position * v->elemSize, elemAddr, v->elemSize);

	return;
}

void VectorInsert(Vector *v, const void *elemAddr, int position){
	if(v->nelem	== v->initialAllocation){
		v->initialAllocation *= 2;
		v->elems = realloc(v->elems, v->initialAllocation * v->elemSize);
	}

	memmove((char *) v->elems + (position + 1) * v->elemSize, (char *) v->elems + (position) * v->elemSize, v->elemSize * (v->nelem - position)); //trato de pasar el arreglo a la siguiente posición
	memcpy((char *) v->elems + position * v->elemSize, elemAddr, v->elemSize); //pasa el valor a la posicion
	v->nelem++;
	return;
}

void VectorNth(const Vector *v, void *elemAddr, int position){
	memcpy(elemAddr, (char *) v->elems + position * v->elemSize,v->elemSize);
	return;
}

void VectorMap(Vector *v, VectorMapFunction mapfn, void *auxData){
	void *elemAddr;
	int i;

	for(i = 0; i < v->nelem; i++){
		elemAddr = (char *)v->elems + i * v->elemSize;
		mapfn(elemAddr, auxData);
	}

	return;
}

void VectorSort(Vector *v, VectorCompareFunction comparefn){
	qsort(v->elems, v->nelem, v->elemSize, comparefn);
	return;
}
